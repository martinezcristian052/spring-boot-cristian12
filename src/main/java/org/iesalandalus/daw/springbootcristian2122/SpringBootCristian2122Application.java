package org.iesalandalus.daw.springbootcristian2122;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCristian2122Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCristian2122Application.class, args);
	}

}
